# Unterlagen Systemtechnik für Energieeffizienz <img src="files/img/label-SYE.png" width=100 />

*Systemtechnik für Energieeffizienz* ist ein Kurs im Master Elektrotechnik und im Master Erneuerbare Energien an der Technischen Hochschule Köln. Der Kurs vermittelt eine systemtechnische Vorgehensweise zur Analyse und Bewertung potenzieller Verbesserungen der Energieeffizienz. Dazu sind die Lehrinhalte in interaktive Jupyter Notebooks aufgeteilt. Weiterhin finden zwei verpflichtende Praktika statt und es gibt ein semesterbegleitendes Projekt. 

## Termine für den laufenden Kurs

In der [Übersicht](http://oer4renewables.gitlab.io/systemtechnik-fuer-energieeffizienz/files/01/SYE-orga.html#termine-und-zugehorige-notebooks) findet sich der laufend aktualisierte Plan. 

Der Kurs findet bis auf weiteres in Präsenz statt. Für ausnahmsweise online-Termine finden Sie den Link in ILIAS (s.u.). 

## Anmelden

- **Prüfung an der TH Köln**: in [PSSO](https://psso.th-koeln.de)
- **Projektteam, Praktikumstermine, Präsentationstermine und Termin für mündliche Prüfung**: in [ILIAS - F07 - Dozenten - May - SYE](https://ilu.th-koeln.de/goto.php?target=crs_17158&client_id=thkilu)
- **python online nutzen**: [jupyterhub](https://jupyterhub.th-koeln.de) erreichbar mit [VPN](https:/www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php), wer im Kurs in ILIAS ist wird dafür freigeschaltet. Eine kurze Nutzungsanleitung gibt es [hier](./files/01/jupyterhub-intro.ipynb). Das Arbeiten mit dem jupyterhub bietet den Vorteil, dass keine eigene python-Installation nötig ist.

## Unterlagen

... werden während des Semesters aktualisiert:

- **[gitlab](https://gitlab.com/oer4renewables/systemtechnik-fuer-energieeffizienz)** Hier können Sie die jeweils aktuellste Version der Unterlagen herunterladen und die Notebooks (`.ipynb`) entweder auf Ihrem eigenen Rechner oder auf dem jupyterhub ausführen. 
- **[html-Buch](http://oer4renewables.gitlab.io/systemtechnik-fuer-energieeffizienz/README.html)** Dieses Buch beinhaltet in strukturierter Version alle Unterlagen. Sie können interaktive Diagramme verwenden oder Code einblenden, jedoch nicht ausführen. 
- **[pdf-Buch](http://oer4renewables.gitlab.io/systemtechnik-fuer-energieeffizienz/sye.pdf)** Dieses Buch beinhaltet in strukturierter Version alle nicht-interaktiv darstellbaren Unterlagen.

## Zitieren

```
@misc{may_2023_987654321,
  author       = {May, Johanna Friederike and Ulrich, André and Tjandra, Steffen Josua and Panteleit, Tobias and Carrasco Serrano, Antonio Francisco and Kartte, Julian},
  title        = {Unterlagen Systemtechnik für Energieeffizienz},
  month        = jan,
  year         = 2023,
  publisher    = {Zenodo},
  doi          = {xx.xxxx/zenodo.xxxxxxx},
  url          = {https://doi.org/xx.xxxx/zenodo.xxxxxxx}
}
```

## Danksagung

Die Materialien sind als open educational resources (OER) verfügbar unter der [Lizenz CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode). Dank einer Förderung im Rahmen des Programms OERcontent.nrw für das Projekt OER4EE (offene Bildungsmaterialien für erneuerbare Energietechnologien) konnten die Materialien weiterentwickelt werden. 

Besonderer Dank gilt meinen Studierenden, die durch ihre Fragen, Anregungen und Ideen zur kontinuierlichen Weiterentwicklung dieses Moduls und zu vielen interessanten fachlichen Diskussionen beitragen. 

![logo OER4EE](files/img/OER4EE_Logos_CC_Lizenz_schmal.jpg)
