{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "(thkoeln)=\n",
    "# Vorstellung der Dozentin und ihrer Erwartungen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Studium"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Studium der Elektrotechnik an der Uni Ulm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{figure} https://upload.wikimedia.org/wikipedia/commons/4/46/Ulm_Donauschwabenufer1.jpg\n",
    "---\n",
    "width: 400px\n",
    "name: fig-Ulm\n",
    "\n",
    "\n",
    "---\n",
    "Ulm (Source: Candidus, Wikimedia Commons, CC0)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Auslandsstudium in Sydney"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    },
    "tags": []
   },
   "source": [
    "<a title=\"Christine Leist, Public domain, via Wikimedia Commons\" href=\"https://commons.wikimedia.org/wiki/File:Sydney_harbourbridge%2Boperahouse.JPG\"><img width=\"512\" alt=\"Sydney harbourbridge+operahouse\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Sydney_harbourbridge%2Boperahouse.JPG/512px-Sydney_harbourbridge%2Boperahouse.JPG\"></a>\n",
    "\n",
    "<p> Bild: Sydney Harbour Bridge und Opernhaus, Christine Leist, Public domain, via Wikimedia Commons </p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Berufserfahrung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Promotion in der Mikrosystemtechnik\n",
    "{cite:p}`may_konzeption_2011`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "![Integration von MEMS in Polymer](../img/promotion_jmay.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Projektleitung Optimale Photovoltaik-Heimspeicher\n",
    "{cite:p}`enargus_verbundvorhaben_2013` {cite:p}`speicher_speicher_2014`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "![Öffentlich geförderter Anteil des Projektes zu Solar-Heimspeichern bei der Robert Bosch GmbH](https://www.saving-volt.de/wp-content/uploads/2014/05/bosch-photovoltaik-energy1.jpg) Bild: {cite:p}`danielb_forschungsprojekt_2014`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "- 1999-2005 Studium der Elektro- und Informationstechnik an der Uni Ulm\n",
    "- 2003 Auslandsstudium in Australien\n",
    "- 2004 Auslandspraktikum in Madagaskar\n",
    "- 2005-2008 Promotion in Mikrosystemtechnik an der Uni Freiburg, durchgeführt bei der Robert Bosch GmbH\n",
    "- 2008-2016 u.a. Photovoltaik-Systemtechnik und Batteriespeicher-Forschung bei der Robert Bosch GmbH\n",
    "- 2016-2017 Forschungsförderung für erneuerbare Energien beim Projektträger Jülich\n",
    "- 2017-heute Professorin an der TH Köln"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Kontakt zur Dozentin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Aktuelle Kontaktdaten auf der TH-Homepage:\n",
    "- [Prof. Dr. Johanna May](https://www.th-koeln.de/personen/johanna.may/)\n",
    "- fachliche und organisatorische Fragen? Gerne gemeinsam diskutieren und beantworten: \n",
    " - in der live-Veranstaltung\n",
    " - im ilias-Forum\n",
    "- persönliche Fragen? Sprechstunde: \n",
    " - Termine + Anmeldung siehe [ilias](https://ilias.th-koeln.de)\n",
    " - Buchung: in ilias in einem meiner Kurse unterhalb vom Minikalender auf Sprechstunde für Prof. May klicken, dann einen Zeitslot auswählen, dann prüfen ob richtig gebucht: im Termin ist automatisch ein zoom-Link drin, wenn nicht, bitte erneut probieren (Termine in Ihrem privaten ilias-Kalender kann ich nicht sehen)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Persönliche Vorstellung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Vorname und Nachname\n",
    "- Studiengang\n",
    "- Was sind Ihre Erfahrungen mit Energieeffizienz, ggf. in beruflichen Umfeldern?\n",
    "- Was erwarten Sie sich von diesem Modul?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition hint\" style=\"background: #e9f6ec; padding: 10px\">\n",
    "<div class=\"title\"><b>Tipp:</b></div>\n",
    "Weiterführendes Interview zu Energieeffizienz (podcast): <a href=\"https://www.enpower-podcast.de/podcast/05-energieeffizienz-dr-clemens-rohde\">enPower - Der Energiewende Podcast: #05 Energieeffizienz - Dr. Clemens Rohde</a> {cite:p}`julius_wesche_05_2020`\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Meine Rolle als Dozentin und meine Erwartungen an Sie"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Meine Rolle:**\n",
    "- inhaltliche Schwerpunkte setzen\n",
    "- Ihre Projektarbeiten begleiten mit Hinweisen und Feedback\n",
    "- Ihren Lernfortschritt bewerten\n",
    "- Nicht: alles wissen\n",
    "- offen für Feedback: kritisches Mitdenken und Einbringen von Erfahrungen und Wissen herzliche willkommen!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Meine Erwartungen:**\n",
    "- selbständige Projektarbeit in Projektteams\n",
    " 1. Teamtermine eigenständig organisieren\n",
    " 2. bei Konflikten im Team ansprechen und klären\n",
    " 3. wenn 2. nicht funktioniert, mit mir sprechen\n",
    "- Praktikum: vorbereitet erscheinen und eigene Ideen / Fragen mitbringen\n",
    "- Exkursion: pünktlich erscheinen, angemessenes Auftreten (Außenwirkung, ggf. zukünftige Masterarbeitsthemen für manche), Interesse mit Fragen und Aufmerksamkeit zeigen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{bibliography}\n",
    ":filter: docname in docnames\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "SYE.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "nbTranslate": {
   "displayLangs": [
    "*"
   ],
   "hotkey": "alt-t",
   "langInMainMenu": true,
   "sourceLang": "en",
   "targetLang": "fr",
   "useGoogleTranslate": true
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
