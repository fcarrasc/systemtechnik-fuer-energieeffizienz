---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

Der Gebäudebereich verursacht einen großen Teil der klimaschädlichen Emissionen. Gleichzeitig werden energetische Aufwände im Rahmen des Klimawandels für Klimatisierung neubewertet. 

+++

Es existieren viele "Rezepte" im Gebäudebereich, die eine klimaschonendere Gebäudenutzung versprechen. Ob diese in konkreten Fällen zu den Nutzeranforderungen passen, ist zu hinterfragen.

+++

Die systemtechnische Methodik lässt sich auch auf die Bewertung der Energieeffizienz von Gebäuden anwenden. 

+++

Im Folgenden finden sich einige Ansatzpunkte. Wichtig ist, diese nicht rezeptartig zu verstehen und einfach Punkt für Punkt anzuwenden, sondern ihre Sinnhaftigkeit für den konkreten betrachteten Fall zu untersuchen.
