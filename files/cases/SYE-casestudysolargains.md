---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Solare Gewinne - nicht immer erwünscht

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['text.usetex'] = True
pd.set_option('display.latex.repr', True)
pd.set_option('display.latex.longtable', True)
locale.setlocale(locale.LC_ALL, '')
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

:::{admonition} Lernziele für diesen Abschnitt:
:class: tip
- was
- Komfortparameter Temperatur, CO2-Gehalt, Feuchte -> Lüften
- womit
- wozu
:::

+++

## Solare Gewinne abschätzen

+++

Fenstergröße, Gebäudeart, etc.

+++

- [ ] TODO Formel und Codebeispiel hinterlegen

+++

## Solare Gewinne verringern

+++

Um solare Gewinne zu verringern, lassen sich ebenfalls kleinere Fenster bauen oder diese so gestalten, dass besonders energiereiche hochstehende Sonneneinstrahlung nicht mehr auftrifft. 

+++

- [ ] TODO Formel und Codebeispiel

+++

Gleichzeitig bedeuten kleine Fenster jedoch auch einen erhöhten künstlichen Beleuchtungsbedarf während der Tagstunden und verringern aus diesem Grunde häufig auch das Wohlbefinden. Daher bleiben gewisse Fensterflächen erhalten, deren Gewächshauswirkung verringert werden muss.

+++

- [ ] TODO Beleuchtungsbedarf Formel mit kleinem Fenster und mit großem Fenster und Codebeispiel

+++

Außenliegender Sonnenschutz (Jalousien, Rollos, Fensterläden) absorbiert selbst oder reflektiert die Sonnenstrahlung und somit gelangt wenig Wärme in den zum Fenster gehörigen Raum. 

+++

- [ ] TODO Formel und Codebeispiel

+++

Innenliegender Sonnenschutz (Jalousien, Rollos, Vorhänge) sollte so ausgelegt sein, dass er die Sonnenstrahlung nicht absorbiert und somit selbst nicht warm wird, sondern dass er die Sonnenstrahlung nach draußen reflektiert. 

+++

- [ ] TODO Formel und Codebeispiel

+++

## Solare Gewinne im Winter optimieren

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
