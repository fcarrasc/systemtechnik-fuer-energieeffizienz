---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

### Ressourceneffizienz

+++ {"slideshow": {"slide_type": "fragment"}}

\begin{equation}
\text{Ressourceneffizienz} = \frac{\text{Nutzen (Produkt, Funktion, funktionale Einheit)}}{\text{Aufwand (Einsatz natürlicher Ressourcen)}}
\end{equation}

+++

<div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>Studie über Ressourceneffizienz bestimmter Produktionsprozesse:</b></div>
    <ul>
        <li> {cite:p}`dana_kralisch_vdi_2015` </li>
    </ul>
</div>

+++ {"slideshow": {"slide_type": "slide"}}

## Lebenszyklus aus ökologischer und aus betriebswirtschaftlicher Sicht

+++ {"slideshow": {"slide_type": "fragment"}}

![Lebenszyklus-Sichtweisen](../img/lebenszyklus.png)

+++

nach {cite:p}`kaltschmitt_umweltbewertung_2015`

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Wo sind Synergien, wo sind Konflikte zwischen den beiden Sichtweisen?
</div>

+++

*Antwort:...*

+++

**Geschäftsmodelle für zirkuläre Wertschöpfung** {cite:p}`hansen_zirkulare_2021`

+++

Hardware klar definieren mit allen Bestandteilen und dadurch Reparierbarkeit einfacher bewertbar machen ist möglich z. B. mit open source Hardware Standard [DIN SPEC 3105 (kostenfrei, CC-Lizenz)](https://www.beuth.de/de/technische-regel/din-spec-3105-1/324805763) {cite:p}`leonhard_dobusch_din_2020`

+++

sehr gute Übersicht über Recyclingquoten und wie diese beeinflusst werden hat {cite:p}`hageluken_recycling_2022`

+++ {"slideshow": {"slide_type": "slide"}}

### Effekte der beiden Lebenszyklus-Sichtweisen

+++ {"slideshow": {"slide_type": "fragment"}}

Gilt für jede (Sub-)Komponente, inklusive der Software, meist jeweils in unterschiedlicher zeitlicher Abfolge!
- **Alterung, teils nutzungsabhängig, teils kalendarisch**
 - Alterung von Komponenten, z. B. verringerte Leuchtkraft
 - Defekte in Komponenten nehmen zu mit der Zeit
- **Technische Obsoleszenz**
 - Software / Hardware veraltet $\to$ Kompatibilität nicht mehr gegeben
- **Reparatur / Ersatz**
 - Ersatzkomponenten ggf. bereits nicht mehr auf dem Markt $\to$ Lagerung von Ersatzkomponenten in manchen Branchen
 - Reparaturkompetenz ggf. nur für teures Geld (wenn überhaupt)
- **Ökonomisches Interesse der Hersteller**

+++ {"slideshow": {"slide_type": "slide"}}

### Zuverlässigkeit im Lebenszyklus (wiederum für jede einzelne (Sub-)Komponente)

+++

<div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>Kurze Einführung in Zuverlässigkeit:</b></div>
{cite:p}`barnard_why_2012`
</div>

+++ {"slideshow": {"slide_type": "fragment"}}

<a title="Bathtub_curve.jpg: Wyatts
derivative work: McSush, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Bathtub_curve.svg"><img width="512" alt="Bathtub curve" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Bathtub_curve.svg/512px-Bathtub_curve.svg.png"></a>

+++

### Sicht der Endanwender auf den Lebenszyklus

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Was wollen Endanwender? Betrachten Sie insbesondere für Ihre Projekte und überlegen Sie, wie Sie diesen Aspekten Rechnung tragen:
    <ul>
        <li> Lieferbarkeit der Komponenten </li>
        <li> Verfügbarkeit von Software-Updates </li>
        <li> Alterung von Komponenten </li>
    </ul>
</div>

+++ {"slideshow": {"slide_type": "fragment"}}

*Antwort:...*

+++ {"slideshow": {"slide_type": "slide"}}

### Bei der Zuverlässigkeit ist es wie bei der Energieeffizienz ...

+++ {"slideshow": {"slide_type": "fragment"}}

... 100% gibt es nicht!

+++ {"slideshow": {"slide_type": "slide"}}

### Zuverlässigkeit und Alterung wirkt sich direkt aus auf ...

+++ {"slideshow": {"slide_type": "fragment"}}

<a title="unbekant, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Magischesdreieck.gif"><img width="256" alt="Magischesdreieck" src="https://upload.wikimedia.org/wikipedia/commons/b/be/Magischesdreieck.gif"></a>

+++ {"slideshow": {"slide_type": "slide"}}

- **Zeit**: 
 - Planungsdauer
 - Stillstandsdauern
 - ...
- **Inhalt und Umfang (Qualität)**: 
 - Zuverlässigkeit
 - Lebensdauer
 - ...
- **Kosten**: 
 - Ersatzteile
 - Lagerung von Ersatzteilen
 - Reparatur (Arbeit)
 - Stillstände
 - ...

+++ {"slideshow": {"slide_type": "fragment"}}

### Anforderung Zuverlässigkeit hat Konsequenzen

+++ {"slideshow": {"slide_type": "fragment"}}

- Stillstandsdauern $\to$ Kosten
 - Servicezeiten (z. B. Reparatur auch nachts)
 - Ersatzteile im Lager vor Ort
 - Ersatzteile regelmäßig auf Funktion prüfen
 - Ersatzteile in korrosionsarmer Umgebung lagern
 - etc.
- Haftung $\to$ Kosten
 - nach Gewährleistungszeitrum: häufig der Kunde
 - Vorsicht: Imageschaden kann höher sein als Haftung
 - Während Gewährleistungszeitraum: häufig der Hersteller bzw. der "In-Verkehr-Bringer"

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Aktualisieren Sie die Anforderungstabelle für Ihre Projekt und ergänzen Sie die Zuverlässigkeitsanforderungen: Wie lange soll das Gerät halten, welche Art von Reparatur muss möglich sein?
</div>

+++ {"slideshow": {"slide_type": "fragment"}}

*Antwort:...*

+++

```{bibliography}
:filter: docname in docnames
```
